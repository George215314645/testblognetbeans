package testblog.DEMO.data;

import java.util.*;

import org.bson.Document;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import testblog.DEMO.components.DataFromBinary;
import testblog.DEMO.controllers.AccessData;


public class AccessMDB implements AccessData {
	private MongoClient mongoClient;
	@Autowired
	private ConfigMDB configMDB;
	@Autowired
	private DataFromBinary dataFromBinary;
	
	public void openConnection() {
		try { 
			mongoClient = new MongoClient(configMDB.host(), configMDB.port());
		} catch (Exception e) {
			System.out.println("Cant open connetcion to mongoDB! " + e.toString());
		}
	}

	@Override
	public Iterable<Document> receiveDocuments(	String docType, 
												String groupId) {
		MongoDatabase database = mongoClient.getDatabase(docType);
		MongoCollection<Document> databaseCollection = database.getCollection(groupId);
		List<Document> outputCollection = new ArrayList<Document>();
		for (Document doc: databaseCollection.find()) {
			Document inDoc = new Document();
			for (String key: doc.keySet()) {
				Object objectInDoc = doc.get(key);
				if (objectInDoc instanceof Binary) {
					String shortName = new StringBuilder().append(doc.get("_id").toString()).append(".").append(key).toString();
					inDoc
					.append(key, dataFromBinary.upload(shortName, (Binary)objectInDoc));
				} else {
					inDoc.append(key, objectInDoc);
				}
			}
			outputCollection.add(inDoc);
		}
		return outputCollection;
	}

	@Override
	public void insertDocument(	Document doc, 
								String docType, 
								String groupId) {
		MongoDatabase database = mongoClient.getDatabase(docType);
		MongoCollection<Document> collection = database.getCollection(groupId);
		collection.insertOne(doc);
	}
	public void closeConnection() {
		mongoClient.close();
	}
}
