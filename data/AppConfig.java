package testblog.DEMO.data;

import testblog.DEMO.components.ConfigFilesDirectory;
import testblog.DEMO.controllers.ConfigBlog;

public class AppConfig implements ConfigMDB, ConfigBlog, ConfigFilesDirectory{
	private String host;
	private int port;
	private String fileUploadPath;
	private String topicsDatabaseName;
	private String commentsDatabaseName;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getFileUploadPath() {
		return fileUploadPath;
	}
	public void setFileUploadPath(String fileUploadPath) {
		this.fileUploadPath = fileUploadPath;
	}
	public String getTopicsDatabaseName() {
		return topicsDatabaseName;
	}
	public void setTopicsDatabaseName(String topicsDatabaseName) {
		this.topicsDatabaseName = topicsDatabaseName;
	}
	public String getCommentsDatabaseName() {
		return commentsDatabaseName;
	}
	public void setCommentsDatabaseName(String commentsDatabaseName) {
		this.commentsDatabaseName = commentsDatabaseName;
	}
	@Override
	public String topic() {
		return topicsDatabaseName;
	}
	@Override
	public String comment() {
		return commentsDatabaseName;
	}
	@Override
	public String host() {
		return host;
	}
	@Override
	public int port() {
		return port;
	}
	@Override
	public String fileUploadPath() {
		return fileUploadPath;
	}
}