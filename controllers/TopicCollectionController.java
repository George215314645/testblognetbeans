package testblog.DEMO.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import testblog.DEMO.components.TestAccount;
import testblog.DEMO.service.Topic;

@Controller
public class TopicCollectionController {
	@Autowired
	private TestAccount account;
	@Autowired
	private AccessData accessData;
	@Autowired
	private ConfigBlog configBlog;
	
	@RequestMapping(value="/topics", method=RequestMethod.GET)
	public String showTopics(	Model model) {
		account.initialisation();
		model.addAttribute("account", account);
		return "topics";
	}
	
	@RequestMapping(value="/topics/new", method=RequestMethod.GET)
	public String newTopicForm() {
		return "new";
	}
	
	@RequestMapping(value="/topics/new", method=RequestMethod.POST)
	public String addTopic(	@RequestParam(value="title", required=true)	String title, 
							@RequestParam(value="text", required=true)	String text, 
							@RequestParam(value="jpg", required=false)	MultipartFile jpg) {
		accessData.insertDocument(	new Topic(title, text, jpg).document(), 
									configBlog.topic(), 
									account.getId());
		return "redirect:/topics";
	}
}