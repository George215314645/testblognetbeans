package testblog.DEMO.controllers;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import testblog.DEMO.components.TestAccount;
import testblog.DEMO.service.Comment;

@Controller
@RequestMapping(value="/topics/show", params="id")
public class SelectTopicController {
	@Autowired
	private TestAccount account;
	@Autowired
	private AccessData accessData;
	@Autowired
	private ConfigBlog configBlog;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public String showTopic(	Model model, 
								@RequestParam(value="id")	String topicId) {
		for (Document doc: account.getTopicCollection()) {
			if (doc.get("_id").toString().equals(topicId)) {
				model.addAttribute("topic", doc);
			}
		}
		model.addAttribute("accountImage", account.getImage());
		model.addAttribute("comments", accessData.receiveDocuments(	configBlog.comment(), 
																	topicId));
		return "show";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String addComment(	@RequestParam(value="message", required=false)	String message,
								@RequestParam(value="id")						String topicId) {
		accessData.insertDocument(	new Comment(account.getName(), message, account.getImage()).document(), 
									configBlog.comment(), 
									topicId);
		return "redirect:/topics/show?id=" + topicId;
	}
}