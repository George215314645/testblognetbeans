package testblog.DEMO.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectController {
	
	@RequestMapping({ "/*", "/topics/*" })
	public String redirect() {
		return "redirect:/topics";
	}
}