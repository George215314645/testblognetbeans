package testblog.DEMO.controllers;

import org.bson.Document;

public interface AccessData {
	Iterable<Document> receiveDocuments(String docType, String groupId);
	void insertDocument(Document doc, String docType, String groupId);
}