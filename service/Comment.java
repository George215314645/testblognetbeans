package testblog.DEMO.service;

import org.bson.Document;

import testblog.DEMO.components.DataFromBinary;

public class Comment extends DocumentSchema implements JspViewer{
	public Comment(String name, String message, String jpg) {
		this.addDate();
		document
		.append("name", name)
		.append("message", message)
		.append("jpg", new DataFromBinary().download(jpg));
	}
	
	public Comment() {}
	@Override
	public String toJsp(Document document) {
		String jpg = document.get("jpg").toString();
		String name = document.get("name").toString();
		String date = document.get("date").toString();
		String message = document.get("message").toString();
		StringBuilder output = new StringBuilder();
		output
		.append("<table>")
			.append("<tr>")
				.append("<td style=\" width: 60px; height: 60px; position: relative; \" >")
					.append("<img src=\" ").append(jpg).append("\" style=\"height: 50px; width: 50px; border-radius: 10px; position: absolute; left: 5px; right: 5px; top: 0px; \" />")
				.append("</td>")
				.append("<td style=\"width: 540px; height: 60px; \" >")
					.append("<b>").append(name).append("</b><br/>")
					.append("<sup>").append(date).append("</sup><br/>")
					.append("<span style=\"margin-right: 100px; \" >").append(message).append("</span>")
				.append("</td>")
			.append("</tr>")
		.append("</table>");	
		return output.toString();
	}
}
