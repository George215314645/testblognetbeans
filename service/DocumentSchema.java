package testblog.DEMO.service;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

import org.bson.Document;

public abstract class DocumentSchema {
	protected Document document;
	protected void addDate() {
		document = new Document();
		LocalDate ld = LocalDate.now();
		StringBuilder date = new StringBuilder()
									.append(ld.getDayOfMonth())
									.append(" ")
									.append(ld.getMonth().getDisplayName(TextStyle.FULL, new Locale("ru", "RU")))
									.append(", ")
									.append(ld.getYear());
		document.append("date", date.toString());
	}
	public Document document() {
		return document;
	}
}
