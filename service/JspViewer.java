package testblog.DEMO.service;

import org.bson.Document;

public interface JspViewer {
	String toJsp(Document document);
}
