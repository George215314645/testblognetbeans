package testblog.DEMO.components;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataFromBinary {
	@Autowired
	ConfigFilesDirectory configFilesDirectory;
	
	public Binary download(String fullName) {
		try {
			InputStream is = new FileInputStream(fullName);
			byte[] data = new byte [is.available()];
			is.read(data);
			is.close();
			return new Binary(data);
		} catch (FileNotFoundException e) {
			System.out.println("Cant open " + fullName + "! " + e.toString());
			return null;
		} catch (IOException e) {
			System.out.println("Cant write data on " + fullName + "! " + e.toString());
			return null;
		}
	}
	public String upload(String shortName, Binary file) {
		if (!new File(configFilesDirectory.fileUploadPath()).exists()) {
			if (!new File(configFilesDirectory.fileUploadPath()).mkdir()) {
				System.out.println("Cant create dirrectory " + configFilesDirectory.fileUploadPath() + "!");
				return null;
			}
		}
		
		String fullName = configFilesDirectory.fileUploadPath() + shortName;
		try {
			OutputStream os = new FileOutputStream(fullName);
			os.write(file.getData());
			os.close();
		} catch (FileNotFoundException e) {
			System.out.println("Cant create file " + fullName + "! " + e.toString());
		} catch (IOException e) {
			System.out.println("Cant download data to file " + fullName + "! " + e.toString());
		}
		return fullName;
	}
}