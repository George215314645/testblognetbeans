package testblog.DEMO.components;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import testblog.DEMO.controllers.AccessData;
import testblog.DEMO.data.AppConfig;

@Component
public class TestAccount {
	@Autowired
	private AccessData accessData;
	@Autowired
	private AppConfig appConfig;
	
	private String id;
	private String name;
	private String image;
	private Iterable<Document> topicCollection;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Iterable<Document> getTopicCollection() {
		return topicCollection;
	}
	public void setTopicCollection(Iterable<Document> topicCollection) {
		this.topicCollection = topicCollection;
	}
	
	public void initialisation() {
		this.id = "TEST_IDaccount";
		this.name = "TEST_Name";
		this.image = appConfig.fileUploadPath() + "TESTaccountImage.jpg";
		this.topicCollection = accessData.receiveDocuments(	appConfig.topic(), 
															this.id);
	}
}