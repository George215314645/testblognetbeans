<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="testblog.DEMO.components.TestAccount"
    import="org.bson.Document"
    import="testblog.DEMO.service.Topic"
%>
<%	Object account = request.getAttribute("account"); 
	int i = 0;
%>
<!DOCTYPE html>
<html>
	<head>
        <style>
        </style>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Все статьи</title>
	</head>
	<body>
		<br/>
		<h1 style="margin-left: 50px" >Все статьи</h1>
		<br/>
		<p><a style="margin-left: 800px" href="/testblog.DEMO/topics/new" >Создать новую статью</a></p>
		<table style="margin-left: auto; margin-right: auto; width: 900px;" ><tr>
		<%	if (account instanceof TestAccount) {
				for (Document doc: ((TestAccount)account).getTopicCollection()) {
					i++;
					if (i == 3) {
						i = 1;
						out.println("</tr><tr>");
					}
					out.println("<td height=\"450px\" width=\"450px\" >");
					out.println(new Topic().toJsp(doc));
					out.println("</td>");
				}
			}
			if (i == 1) {
				out.println("</td><td>");
			}
		%>
		</tr></table>
		<%	if (i == 0) {
				out.println("<i style=\"margin-left: 50px\" >Нет статей</i>");
			} 
		%>
	</body>
</html>