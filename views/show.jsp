<?xml version="1.0" encoding="UTF-8" ?>
<%@page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
    import="testblog.DEMO.components.TestAccount"
    import="org.bson.Document" 
    import="testblog.DEMO.service.Comment"
%>
<%	Document topic;
	if (request.getAttribute("topic") instanceof Document) {
		topic = (Document)(request.getAttribute("topic"));
	} else {
		System.out.println("topic document null");
		topic = new Document();
	}
%>
<!DOCTYPE html>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>
			<%=	topic.get("title").toString() %>
		</title>
	</head>
	<body>
		<article style="margin-left: auto; margin-right: auto; width: 600px; border:1px grey solid; border-radius: 10px; " >
			<img src="<%= topic.get("jpg") %>" style="border-radius: 10px 10px 0 0; height: 225px; width: 600px;" />
			<h2 style="padding: 20px 20px 20px 20px;" >
				<%=	topic.get("title") %>
			</h2>
			<sub style="padding: 20px 20px 20px 20px;" >
				<%= topic.get("date") %>
			</sub><hr/>
			<p style="padding: 20px 20px 20px 20px;" >
				<%= topic.get("text") %>
			</p>
		</article>
		<br/>
		<article style=" margin-left: auto; margin-right: auto; width: 600px; " >
			<form method="post" >
			<table>
				<tr>
					<td style="width: 60px; height: 60px; " ><img src="<%= request.getAttribute("accountImage") %>" style="height: 50px; width: 50px; margin: 0px; border-radius: 10px; " /></td>
					<td style="width: 430px; height: 60px; " ><TEXTAREA placeholder="Оставить комментарий" name="message" rows="4" cols="50"  ></TEXTAREA><br/></td>
					<td style="width: 110px; height: 60px; " ><input style="width: 90px; height: 60px;" type="submit" value="Отправить" ></input></td>
				</tr>
			</table>
			</form>
		</article>
		<br/>
		<article style=" margin-left: auto; margin-right: auto; width: 600px; " >
			<%	Object comments = request.getAttribute("comments");
				if (comments instanceof Iterable<?>) {
					for (Object doc: (Iterable<?>)comments) {
						if (doc instanceof Document) {
							out.println(new Comment().toJsp((Document)doc));
							out.println("<br/>");
						}
					}
				}
			%>
		</article>
	</body>
</html>